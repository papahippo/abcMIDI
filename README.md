# abcMIDI
Please note that the official abcmidi project is maintained by Seymour Shlien and hosted at
*https://github.com/sshlien/abcmidi*. 
This project was forked from there (many years after the assosciated code was written!) in order to co-ordinate and document a quality improvement activity. The initial instigation of this activity was the large number of compiler warnings when generating abcmidi and 'friends', but it soon became apparent that a number of real defects needed to be addressed.

All the code is currently in the 'abcmidi' subdirectory of this project directory. You will also find a rather out-of-date "readme" there.
